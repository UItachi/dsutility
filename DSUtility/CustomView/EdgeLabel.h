//
//  EdgeLabel.h
//  DragonSourceCommon
//
//  Created by WeiHan on 2/1/16.
//  Copyright © 2016 DragonSource. All rights reserved.
//
//  http://stackoverflow.com/a/21267507/1677041
//

#import <UIKit/UIKit.h>

@interface EdgeLabel : UILabel

@property (nonatomic) UIEdgeInsets textInsets;

@end
