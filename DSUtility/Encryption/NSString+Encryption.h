//
//  NSString+Encryption.h
//  youyue
//
//  Created by WeiHan on 7/19/16.
//  Copyright © 2016 DragonSource. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Encryption)

- (NSString *)encryptDESStringWithKey:(const char *)key iv:(const char *)iv;

- (NSString *)md5Result;

- (NSString *)encryptBySHA1;

@end
