//
//  NSData+Encryption.m
//  DragonSourceCommon
//
//  Created by WeiHan on 2/26/16.
//  Copyright © 2016 DragonSource. All rights reserved.
//

#import "NSData+Encryption.h"
#import <CommonCrypto/CommonCryptor.h>

@implementation NSData (Encryption)

#pragma mark - AES128

- (NSData *)encryptAES128WithKey:(NSString *)key
{
    char keyPtr[kCCKeySizeAES256 + 1];

    bzero(keyPtr, sizeof(keyPtr));
    [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];

    NSUInteger dataLength = [self length];
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc(bufferSize);
    size_t numBytesEncrypted = 0;

    CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt, kCCAlgorithmAES128,
                                          kCCOptionPKCS7Padding | kCCOptionECBMode,
                                          keyPtr, kCCBlockSizeAES128,
                                          NULL,
                                          [self bytes], dataLength,
                                          buffer, bufferSize,
                                          &numBytesEncrypted);

    NSData *resultData = nil;

    if (cryptStatus == kCCSuccess) {
        resultData = [NSData dataWithBytes:buffer length:numBytesEncrypted];
    }

    free(buffer);

    return resultData;
}

- (NSData *)decryptAES128WithKey:(NSString *)key
{
    char keyPtr[kCCKeySizeAES256 + 1];

    bzero(keyPtr, sizeof(keyPtr));
    [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];

    NSUInteger dataLength = [self length];
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc(bufferSize);
    size_t numBytesDecrypted = 0;

    CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt, kCCAlgorithmAES128,
                                          kCCOptionPKCS7Padding | kCCOptionECBMode,
                                          keyPtr, kCCBlockSizeAES128,
                                          NULL,
                                          [self bytes], dataLength,
                                          buffer, bufferSize,
                                          &numBytesDecrypted);

    NSData *resultData = nil;

    if (cryptStatus == kCCSuccess) {
        resultData = [NSData dataWithBytes:buffer length:numBytesDecrypted];
    }

    free(buffer);

    return resultData;
}

#pragma mark - AES256

- (NSData *)encyptAES256WithKey:(const char *)key iv:(const char *)iv
{
    NSUInteger dataLength = [self length];

    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc(bufferSize);

    size_t numBytesEncrypted    = 0;
    CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding,
                                          key, kCCKeySizeAES256,
                                          iv,
                                          [self bytes], dataLength, /* input */
                                          buffer, bufferSize, /* output */
                                          &numBytesEncrypted);
    NSData *resultData = nil;

    if (cryptStatus == kCCSuccess) {
        resultData = [NSData dataWithBytes:buffer length:numBytesEncrypted];
    }

    free(buffer);

    return resultData;
}

- (NSData *)decryptAES256WithKey:(const char *)key iv:(const char *)iv
{
    NSUInteger dataLength = [self length];

    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc(bufferSize);

    size_t numBytesDecrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding,
                                          key, kCCKeySizeAES256,
                                          iv,
                                          [self bytes], dataLength, /* input */
                                          buffer, bufferSize, /* output */
                                          &numBytesDecrypted);

    NSData *resultData = nil;

    if (cryptStatus == kCCSuccess) {
        resultData = [NSData dataWithBytes:buffer length:numBytesDecrypted];
    }

    free(buffer);
    return resultData;
}

#pragma mark - DES

- (NSData *)encryptDESWithKey:(NSString *)key
{
    NSData *sourceData = self;
    NSInteger d = [sourceData length] % kCCBlockSizeDES;
    NSMutableData *cipherData = [NSMutableData dataWithLength:[self length] + (kCCBlockSizeDES - d)];

    size_t numBytesEncrypted = 0;

    const char *iv = [key UTF8String];

    CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt, kCCAlgorithmDES,
                                          kCCOptionPKCS7Padding, //kCCOptionPKCS7Padding | kCCModeCBC,
                                          [key UTF8String], kCCKeySizeDES,
                                          iv,
                                          [sourceData bytes], [sourceData length],
                                          cipherData.mutableBytes, [cipherData length],
                                          &numBytesEncrypted);

    if (cryptStatus == kCCSuccess) {
        return cipherData;
    }

    return nil;
}

- (NSData *)decryptDESWithKey:(NSString *)key
{
    NSData *sourceData = self;
    NSMutableData *plainData = [NSMutableData dataWithLength:(sourceData.length + kCCBlockSizeDES)];

    size_t numBytesDecrypted = 0;

    const char *iv = [key UTF8String];
    CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt, kCCAlgorithmDES,
                                          0, //kCCOptionPKCS7Padding | kCCModeCBC,
                                             //kCCModeCBC|0x0000,
                                             //kCCOptionECBMode|kCCOptionPKCS7Padding,
                                          [key UTF8String], kCCKeySizeDES,
                                          iv,
                                          [sourceData bytes], sourceData.length,
                                          [plainData mutableBytes], plainData.length,
                                          &numBytesDecrypted);

    if (cryptStatus == kCCSuccess) {
        NSData *resultData = [NSMutableData dataWithBytes:plainData.mutableBytes length:numBytesDecrypted];
        return resultData;
    }

    return nil;
}

@end
