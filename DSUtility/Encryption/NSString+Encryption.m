//
//  NSString+Encryption.m
//  youyue
//
//  Created by WeiHan on 7/19/16.
//  Copyright © 2016 DragonSource. All rights reserved.
//

#import "NSString+Encryption.h"
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonDigest.h>


@implementation NSString (Encryption)

- (NSString *)encryptDESStringWithKey:(const char *)key iv:(const char *)iv
{
    NSData *sourceData = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSInteger unit = 8;
    NSInteger d = [sourceData length] % unit;
    NSMutableData *cipherData = [NSMutableData dataWithLength:[sourceData length] + (unit - d)];

    size_t numBytesEncrypted = 0;
    NSString *encryptedStr;

    CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt, kCCAlgorithmDES,
                                          kCCOptionPKCS7Padding,
                                          key, kCCKeySizeDES,
                                          iv,
                                          [sourceData bytes], [sourceData length],
                                          cipherData.mutableBytes, [cipherData length],
                                          &numBytesEncrypted);

    if (cryptStatus == kCCSuccess) {
        encryptedStr = [cipherData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    }

    return encryptedStr;
}

- (NSString *)md5Result
{
    const char *cStr = [self UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];

    CC_MD5(cStr, (CC_LONG)strlen(cStr), digest);

    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];

    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }

    return output;
}

- (NSString *)encryptBySHA1
{
    const char *cstr = [self cStringUsingEncoding:NSUTF8StringEncoding];

    NSData *data = [NSData dataWithBytes:cstr length:self.length];

    uint8_t digest[CC_SHA1_DIGEST_LENGTH];

    CC_SHA1(data.bytes, (int)data.length, digest);

    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];

    for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }

    return output;
}

@end
