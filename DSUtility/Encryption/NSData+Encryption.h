//
//  NSData+Encryption.h
//  DragonSourceCommon
//
//  Created by WeiHan on 2/26/16.
//  Copyright © 2016 DragonSource. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Encryption)

#pragma mark - AES128

/**
 *    @brief  encrypt/decrypt method doesn't have Base64 encode/decode within its implementation but you need it manually alone!
 *          Besides, iv (initialization vector) is ignored here.
 *
 */
- (NSData *)encryptAES128WithKey:(NSString *)key;
- (NSData *)decryptAES128WithKey:(NSString *)key;

#pragma mark - AES256

- (NSData *)encyptAES256WithKey:(const char *)key iv:(const char *)iv;
- (NSData *)decryptAES256WithKey:(const char *)key iv:(const char *)iv;

#pragma mark - DES

- (NSData *)encryptDESWithKey:(NSString *)key;
- (NSData *)decryptDESWithKey:(NSString *)key;

@end
