//
//  DSLog.h
//
//  Created by WeiHan on 2018/11/23.
//

/**
 *
 * Usage Steps:
 *
 * Step 1:
 *  #import <DSLog.h>
 *
 * Step 2:
 *  #define DS_LOG_LEVEL_DEF moduleLogLevel
 *
 * Step 3:
 *  For the constant log level case, define it as `static const DDLogLevel moduleLogLevel = DDLogLevelVerbose;
 *  For the dynamic log level case, declare it as `FOUNDATION_EXPORT DDLogLevel moduleLogLevel;` in header file
 *      and define it in source file implementation.
 *
 *  Notes:
 *
 *  1. For the library use case, it'd better import the header file and define the level variable in prefix header file,
 *
 *  2. Do not use the `static DDLogLevel moduleLogLevel = DDLogLevelVerbose;` in any header files for dynamic
 *      log level case. It will be compiled into different source files with independent global variables.
 *
 **/
#import <CocoaLumberjack/CocoaLumberjack.h>

#ifdef DS_LOG_LEVEL_DEF
    #error "Do not define this log level multiple times or define it before importing this header file!"
#endif

#define DSLogError(frmt, ...)   LOG_MAYBE(NO,                DS_LOG_LEVEL_DEF, DDLogFlagError,   0, nil, __PRETTY_FUNCTION__, frmt, ## __VA_ARGS__)
#define DSLogWarn(frmt, ...)    LOG_MAYBE(LOG_ASYNC_ENABLED, DS_LOG_LEVEL_DEF, DDLogFlagWarning, 0, nil, __PRETTY_FUNCTION__, frmt, ## __VA_ARGS__)
#define DSLogInfo(frmt, ...)    LOG_MAYBE(LOG_ASYNC_ENABLED, DS_LOG_LEVEL_DEF, DDLogFlagInfo,    0, nil, __PRETTY_FUNCTION__, frmt, ## __VA_ARGS__)
#define DSLogDebug(frmt, ...)   LOG_MAYBE(LOG_ASYNC_ENABLED, DS_LOG_LEVEL_DEF, DDLogFlagDebug,   0, nil, __PRETTY_FUNCTION__, frmt, ## __VA_ARGS__)
#define DSLogVerbose(frmt, ...) LOG_MAYBE(LOG_ASYNC_ENABLED, DS_LOG_LEVEL_DEF, DDLogFlagVerbose, 0, nil, __PRETTY_FUNCTION__, frmt, ## __VA_ARGS__)

#ifdef DDLogError
#undef DDLogError
#endif

#ifdef DDLogWarn
#undef DDLogWarn
#endif

#ifdef DDLogInfo
#undef DDLogInfo
#endif

#ifdef DDLogDebug
#undef DDLogDebug
#endif

#ifdef DDLogVerbose
#undef DDLogVerbose
#endif

#define DDLogError(frmt, ...)   _Pragma("GCC warning \"Replace 'DDLogError' with 'DSLogError'\"") DSLogError(frmt, ## __VA_ARGS__)
#define DDLogWarn(frmt, ...)    _Pragma("GCC warning \"Replace 'DDLogWarn' with 'DSLogWarn'\"") DSLogWarn(frmt, ## __VA_ARGS__)
#define DDLogInfo(frmt, ...)    _Pragma("GCC warning \"'Replace 'DDLogInfo' with 'DSLogInfo'\"") DSLogInfo(frmt, ## __VA_ARGS__)
#define DDLogDebug(frmt, ...)   _Pragma("GCC warning \"Replace 'DDLogDebug' with 'DSLogDebug'\"") DSLogDebug(frmt, ## __VA_ARGS__)
#define DDLogVerbose(frmt, ...) _Pragma("GCC warning \"Replace 'DDLogVerbose' with 'DSLogVerbose'\"") DSLogVerbose(frmt, ## __VA_ARGS__)
